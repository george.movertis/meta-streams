import {

    HomeDemo3FeaturesS1,
    HomeDemo3FeaturesS2,
    HomeDemo3FeaturesS3,
    HomeDemo3FeaturesS4,

    HomeDemo3FeaturesF1,
    HomeDemo3FeaturesF2,
    HomeDemo3FeaturesF3,
    HomeDemo3FeaturesF4,
    HomeDemo3FeaturesF5,
    HomeDemo3FeaturesF6,

    TeamImg1,
    TeamImg2,
    TeamImg3,
    TeamImg4,
    TeamImg5,
    TeamImg6,

    HomeDemo3BlogImg1,
    HomeDemo3BlogImg2,
    HomeDemo3BlogImg3,

} from '../../../utils/allImgs'

export const TokenFeaturesTop = [
    {
        img:HomeDemo3FeaturesF1,
        title:"Delivery Reports"
    },
    {
        img:HomeDemo3FeaturesF2,
        title:"Branded Sender ID"
    }
]

export const TokenFeaturesMed = [
    {
        img:HomeDemo3FeaturesF3,
        title:"Marketing Campigns"
    },
    {
        img:HomeDemo3FeaturesF4,
        title:"professional Routing"
    },
]
export const TokenFeaturesDown = [
    {
        img:HomeDemo3FeaturesF5,
        title:"Traking API"
    },
    {
        img:HomeDemo3FeaturesF6,
        title:"Two-Way Messaging"
    }
]

export const SmartContractinfo = [
    {
        img:HomeDemo3FeaturesS1,
        title:"VIP Discord with exclusive memes, technical analysis & more"

    },
    {
        img:HomeDemo3FeaturesS2,
        title:"Unique Profile Picture with exclusive rarity"

    },
    {
        img:HomeDemo3FeaturesS3,
        title:"Exclusive Drops regulary"

    },
    {
        img:HomeDemo3FeaturesS4,
        title:"Being part of the best NFT meme project"

    },
]

export const OurTeamInfo = [
    {
        img:TeamImg1,
        title:"Christian Hardin",
        text:"CEO"
    },
    {
        img:TeamImg2,
        title:"Lucy Moyer",
        text:"Business Development"
    },
    {
        img:TeamImg3,
        title:"Alex Garcia",
        text:"UX/UI Designer"
    },
    {
        img:TeamImg4,
        title:"George Walls",
        text:"Tech Developer"
    },
    {
        img:TeamImg5,
        title:"Lucas Hanson",
        text:"Marketing"
    },
    {
        img:TeamImg6,
        title:"Noah Myers",
        text:"Marketing"
    }
]

export const OurBlogInfo = [
    {
        img:HomeDemo3BlogImg1,
        title:"What is this Token for?."
    },
    {
        img:HomeDemo3BlogImg2,
        title:"The most powerful Token"
    },
    {
        img:HomeDemo3BlogImg3,
        title:"How to get trial version"
    }
]