import SectionHeading from "../SectionHeading"

const Roadmap = ({data , ClassSpanTitle}) => {

    return (

      <section className="roadmap section-padding-0-0 roadmapPers "  id="roadmap">
        <SectionHeading
          title="Our Roadmap"
          // text="Our ICO Roadmap"
          ClassSpanTitle={ClassSpanTitle}
        />
        <div className="container " data-aos="fade-up">
          <div className="row">
            <div className="col-md-12">
              <div className="main-timeline">
                {data && data.map((item , key) => (
                  <div className="timeline" key={key}>
                    <div className="icon" />
                    <div className="date-content">
                      <div className="date-outer"> <span className="date"> <span className="month">{item.month}</span> <span className="year">{item.year}</span> </span>
                      </div>
                    </div>
                    <div className="timeline-content">
                      <h4 className="descr">{item.title}</h4>
                      <p className="descr"> {item.description}</p>
                    </div>
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>
      </section>
    );

}

export default Roadmap