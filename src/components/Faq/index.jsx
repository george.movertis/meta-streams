import {
  HomeDemo1Faq
} from "../../utils/allImgs"

import SectionHeading from "../SectionHeading"

const Faq = ({data , ClassSpanTitle}) => {

    return (

      <section className="faq-timeline-area section-padding-100-100  holders">
        <div className="container "  id="faq">
          <SectionHeading
            title="Frequently Asked Questions"
            text="Frequently Questions"
            ClassSpanTitle={ClassSpanTitle}
          />

          <div className="row align-items-center"  data-aos="fade-up">
            <div className="col-12 col-lg-6 offset-lg-0 col-md-8 offset-md-2 col-sm-12">
              <img draggable="false" src={HomeDemo1Faq} alt="" className="center-block img-responsive" />
            </div>
            <div className="col-12 col-lg-6 col-md-12">
            <div className="dream-faq-area mt-s ">
                <dl id="basicAccordion">
                   {/* {data && data.map((item , key) => (
                    <>
                      <dt className="wave" data-bs-toggle="collapse" data-bs-target={`#${item.ID}`} aria-expanded="false">{item.text}</dt>
                      <dd data-aos="fade-up" id={item.ID} aria-labelledby="headingOne" data-bs-parent="#basicAccordion" className="accordion-collapse collapse">
                          <p className="accordion-body">{item.answer}</p>
                      </dd>
                    </>
                  ))}  */}



                </dl>

<input type="checkbox" id="title1" />
<label className="descr" for="title1">When I will get my lambo?</label>
<div class="content">
<p2 className="faq">Only the true diamond hands holders will make this dream come true.</p2>
</div>

<input type="checkbox" id="title2" />
<label className="descr" for="title2">When is the minting going live?</label>
<div class="content">
<p2 className="faq">The minting will go live in the first quarter of 2022.</p2>
</div>

<input type="checkbox" id="title3" />
<label className="descr" for="title3">How many can I mint?</label>
<div class="content">
<p2 className="faq">With the same wallet you can mint 20 NFTs.</p2>
</div>

<input type="checkbox" id="title4" />
<label className="descr" for="title4">What is the contract?</label>
<div class="content">
<p2 className="faq">The contract is on the ETH blockchain. <br></br>The contract: 0xfB938e4bEB11FAE11Ea3D8f33CE1ecB11d971100. </p2>
</div>
                </div>

            </div>
          </div>
        </div>
      </section>
    );

}

export default Faq