import Contract from "./Contract"
import SectionHeading from "../../components/SectionHeading"

import {
  WSBHolders
} from '../../utils/allImgs'
const SmartContract = ({data, ClassSpanTitle}) => {

    return (

      <section className="smart-contract holders section-padding-100 clearfix" >
        <div className="container" id="about">
        <SectionHeading
            title="Why Join WSB Degenerates Club?"
            // text="Our Trading Platform"
            descr="At WallStreetBets Degenerates Club we want to create a strong community passionate about investments and memes.
           The value you will get for belonging to this community goes far beyond a profile picture."
            ClassSpanTitle={ClassSpanTitle}
          />
          <div className="row " data-aos="fade-up" >
            <div className="col-12 col-lg-6 offset-lg-0">
              <div className="who-we-contant">
                {/* <div className="dream-dots text-left fadeInUp" data-wow-delay="0.2s">
                  <span className="gradient-text blue">Smart Contract</span>
                </div>
                <h4 className="fadeInUp" data-wow-delay="0.3s">Comprehensive smart contracts.</h4> */}
                {/* <p className="fadeInUp" data-wow-delay="0.4s">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis at dictum risus, non suscipit arcu. Quisque aliquam posuere tortor, sit amet convallis nunc scelerisque in.</p>
                <p className="fadeInUp" data-wow-delay="0.5s">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit ipsa ut quasi adipisci voluptates, voluptatibus aliquid alias beatae reprehenderit incidunt iusto laboriosam.</p>
                <a className="btn more-btn mt-30 fadeInUp" data-wow-delay="0.6s" href="#">Read More</a> */}
                  <div className="service-img-wrapper col-lg-10 col-md-10 col-sm-10 mt-s">
                    <div className="image-box">
                      <img style={{maxWidth:"130%", marginTop:"-80px"}} draggable="false" src={WSBHolders} className="center-block img-responsive phone-img" alt="" />
                    </div>
                  </div>  
              </div>
            </div>
            <div className="col-12 col-lg-6 offset-lg-0 mt-s" >
              {data && data.map((item , key) => (
                <Contract
                  key={key}
                  img={item.img}
                  title={item.title}
                />
              ))}
            </div>
          </div>
        </div>
      </section>

      
    );

}

export default SmartContract;