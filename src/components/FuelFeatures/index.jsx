import {HomeDemo2Feature1 , HomeDemo2SmallCar} from '../../utils/allImgs'
import ActionBuy from '../Buy';

const FuelFeatures = () => {

    return (

      <section className="special fuel-features section-padding-100 clearfix">
        <div className="container has-shadow">
          <div className="row align-items-center">
            <div className="col-12 col-lg-6 offset-lg-0 col-md-12 no-padding-left">
              <div className="welcome-meter mb-30">
                <img draggable="false" src={HomeDemo2Feature1} alt="" />
              </div>
            </div>
            <div className="col-12 col-lg-6 offset-lg-0">
              <div className="who-we-contant">
                <div className="dream-dots text-left">
                  <span className="gradient-text blue">Crypto Trading</span>
                </div>
                <ActionBuy/>
              </div>
            </div>
          </div>
        </div>
      </section>
    );

}

export default FuelFeatures;