import {useEffect} from "react";
import {addRemoveClassBody} from '../../utils'
import {Provider} from "react-redux";
import store from "../../redux/store";
import ActionBuy from "../../components/Buy";

import {
        TokenFeaturesTop,
        TokenFeaturesMed,
        TokenFeaturesDown,
        SmartContractinfo,
        OurTeamInfo,
        OurBlogInfo
       } from '../../data/data-containers/HomeDemo3/data-HomeDemo3.js';

import OurPatformInfo from '../../data/data-containers/HomeDemo3/data-OurPatformInfo.json';

import FeaturesOtherTop from '../../data/data-containers/HomeDemo3/data-FeaturesOtherTop.json';
import FeaturesOtherDown from '../../data/data-containers/HomeDemo3/data-FeaturesOtherDown.json';
import TokenDistributionInfo from '../../data/data-containers/HomeDemo3/data-TokenDistributionInfo.json';
import RoadmapInfo from '../../data/data-containers/HomeDemo3/data-RoadmapInfo.json';
import FaqInfo from '../../data/data-containers/HomeDemo3/data-FaqInfo.json';

import {
        HomeDemo3Wwhitepaper,
        HomeDemo3About1
        } from '../../utils/allImgs'

import './style/HomeDemo3.scss'

import Header from "../../layouts/Header"
import FooterPages from '../../layouts/Footer/FooterPages'

import SecHeroSection from '../../components/HeroSection'
import SecAbout from '../../components/SecAbout'
import FuelFeatures from '../../components/FuelFeatures'
import Features2 from '../../components/Features2'
import SpreadMap from '../../components/SpreadMap'
import SmartContract from '../../components/SmartContract'
import TokenDistribution from '../../components/TokenDistribution'
import Roadmap from '../../components/Roadmap'
import Faq from '../../components/Faq'
import OurTeam from '../../components/OurTeam'
import Subscribe from '../../components/Subscribe'
import OurBlog from '../../components/OurBlog'

import OurPlatform from './OurPlatform'
import OurPlatform2 from './OurPlatform2'

import TokenFeatures from "./TokenFeatures"



const HomeDemo3Container = () => {

    useEffect(() => {
            addRemoveClassBody('darker')
          },[])  

    return (
      <div>
        <Provider store={store}><Header Title="WSB Degenerates Club" /></Provider>
        <SecHeroSection
          ClassSec="hero-section de-3 section-padding"
          ClassDiv="col-md-6 col-md-6 col-md-12"
          specialHead="Break The Rules"
          title="Welcome To The &#13;&#10; Best NFT Meme Club"
          link1="DISCORD"
          link2="TWITTER"
          link3="INSTAGRAM"
          link4="FACEBOOK"

         
          HomeDemo1Or4Or5Or6={false}         
        />    
        <div className="clearfix" />

        <SmartContract data={SmartContractinfo} />

        <div className="clearfix" />
        {/* <Provider store={store}>           
<FuelFeatures />
</Provider> */}
         <Roadmap
            data={RoadmapInfo}
            ClassSpanTitle="w-text"
        />
        <div className="clearfix" />

        <Faq
            data={FaqInfo}
        />
        <div className="clearfix" />

        {/* <OurPlatform
            data={OurPatformInfo}
        /> */}
         <div className="clearfix" />


        <OurTeam
            data={OurTeamInfo}
            ClassSpanTitle="gradient-text black"

        />
        
         <FooterPages/> 
        {/* <OurPlatform2
            data={OurPatformInfo}  
        /> */}

        {/* <Provider store={store}>
            <ActionBuy ClassSpanTitle="gradient-text blue"/>
        </Provider> */}
          {/* <SecAbout
          imgDwon={true}
          title=" Best Trading Experience "
          text=" Trade crypto currency and refer new members to get bounes."
          img={HomeDemo3About1}
        />  */}
     {/* <FuelFeatures />
        <TokenFeatures
            TokenFeaturesTop={TokenFeaturesTop}
            TokenFeaturesMed={TokenFeaturesMed}
            TokenFeaturesDown={TokenFeaturesDown}
            ClassSpanTitle="gradient-text blue"
        />
        <SpreadMap
            Wwhitepaper={HomeDemo3Wwhitepaper}
        />
        <TokenDistribution
            data={TokenDistributionInfo}
            ClassSpanTitle="gradient-text blue"
        />
        <Features2
            icoCounterClass="ico-counter dotted-bg mb-30"
            addOther={false}
            FeaturesOtherTop={FeaturesOtherTop}
            FeaturesOtherDown={FeaturesOtherDown}
        />
        
       
        <Subscribe />
        <OurBlog
            data={OurBlogInfo}
            ClassSpanTitle="gradient-text blue"
        />   */} 
        
      </div>
    );
};

export default HomeDemo3Container