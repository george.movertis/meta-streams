import {
    HomeDemo3WSBFU,
    HomeDemo3Rings
} from '../../../utils/allImgs'

import SectionHeading from "../../../components/SectionHeading"
import {Provider} from "react-redux";
import store from "../../../redux/store";
import ActionBuy from "../../../components/Buy";
const OurPlatform = ({data , ClassSpanTitle}) => {

    return (

      <section class="features section-padding-100 holders">
        <div className="container">
          <SectionHeading
            title="Why Join WST Degenerates Club?"
            // text="Our Trading Platform"
            descr="At WallStreetBets Degenerates Club we want to create a strong community passionate about investments and memes. 
            The value you will get for belonging to this community goes far beyond a profile picture."
            ClassSpanTitle={ClassSpanTitle}
          />
          <div className="row align-items-center" data-aos="fade-up" >

          <div className="service-img-wrapper how col-lg-5 col-md-9 col-sm-12 mt-s ">
            <div className="features-list v2">
              <div className="who-we-contant">
                <h4 className="w-text ">For our holders</h4>
                <p className="w-text ">We want true diamond hands holders on our community. That's why we're creating a long term project with amazing benefits for you. </p>
              </div>
              <ul className="list-marked">
                {data && data.map((item , key) => (
                    <li key={key} className="text-white"><i className="fa fa-check" />{item.NameMarked}</li>
                ))}
              </ul>
            </div>            
          </div>
          
            <div className="service-img-wrapper col-lg-7 col-md-12 col-sm-12 mt-s">
              <div className="image-box">
                <img draggable="false" src={HomeDemo3WSBFU} className="center-block img-responsive phone-img" alt="" />
                <img draggable="false" src={HomeDemo3Rings} className="center-block img-responsive rings " alt="" />
              </div>
            </div>  
            
           
          </div>
        </div>
        

      </section>
    );
}

export default OurPlatform;